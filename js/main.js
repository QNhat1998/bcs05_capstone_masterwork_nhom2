var btn = $('.backToTop');

$(window).scroll(function () {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

btn.on('click', function (e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, '300');
});

$(window).on("scroll", function () {
    var scroll = $(window).scrollTop();

    if (scroll >= 80) {
        $("#header-scroll").addClass("nav-fixed");
    } else {
        $("#header-scroll").removeClass("nav-fixed");
    }

});
$(document).ready(function () {
    $(".counter").each(function () {
        var count = $(this);
        var countTo = count.attr('data-count');
        // console.log(countTo);
        $({ countNum: count.text() }).animate({
            countNum: countTo,
        },
            {

                duration: 1000,
                easing: 'linear',
                step: function () {
                    count.text(Math.floor(this.countNum));
                },
                complete: function () {
                    count.text(this.countNum);
                }
            });
    });
});
const element = document.getElementById('darkMode')

darkTheme = () => {
    if (element.classList.contains('dark-mode')) {
        element.classList.remove('dark-mode')
        element.classList.add('light-mode')
        document.getElementById("btn-switch").classList.remove('dark-theme')
        localStorage.setItem("theme", "light-mode")
        localStorage.setItem("switch", "")
    } else {
        element.classList.remove('light-mode')
        element.classList.add('dark-mode')
        document.getElementById("btn-switch").classList.add('dark-theme')
        localStorage.setItem("theme", "dark-mode")
        localStorage.setItem("switch", "dark-theme")
    }
}

element.classList.add(localStorage.getItem("theme"))
document.getElementById("btn-switch").classList.add(localStorage.getItem("switch"))
